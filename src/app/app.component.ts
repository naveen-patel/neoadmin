
import { Component, OnInit, ChangeDetectorRef, OnDestroy, AfterContentChecked } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterContentChecked {
  mobileQuery: MediaQueryList;
  login = false;
  loggedin = false;

  private mobileQueryListene: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private route: Router
    ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListene = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListene);
  }
// tslint:disable-next-line:use-life-cycle-interface
ngOnInit() {
  console.log('init');
}


ngAfterContentChecked() {
}

check() {
  console.log('check');
  if ((this.route.url === '/') || (this.route.url === '/login')) {
    console.log('path', this.route.url);
    this.login = true;
    this.loggedin = false;
    return true;
  } else {
    this.login = false;
    this.loggedin = true;
    return true;
  }
}
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this.mobileQueryListene);
  }
}
