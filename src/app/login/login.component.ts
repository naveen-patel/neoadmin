import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]);
  password = new FormControl('', [Validators.required, Validators.pattern(/^(?=.*\d).{5,16}$/)]);
  hide = true;
  text = false;
  constructor(
    private route: Router
    ) { }
  ngOnInit() {

  }

  getErrorMessageaMail() {
    return this.email.hasError('required') ? 'You must enter a value' :
      this.email.hasError('pattern') ? 'Not a valid email' :
        '';
  }
  getErrorMessageaPassword() {
    return this.password.hasError('required') ? 'You must enter a value' :
      this.password.hasError('pattern') ? 'Password must be between 4 and 16 digits long and include at least one numeric digit' :
        '';
  }
  submit() {
    if (this.email.valid === true && this.password.valid === true) {
      return false;
    } else {
      return true;
    }
  }
  onSubmit(): void {
    console.log('on submit');
    if (this.email.valid === false || this.password.valid === false) {
      alert('fill the form properly!!!');
    } else {
      this.text = !this.text;
      this.route.navigate(['/dashboard']);
    }
  }
}
